# hello-activitypub
Following the 
 basic ActivityPub walk through
 and absorbing [spec](https://www.w3.org/TR/activitypub/)
 in morsels


<a href="https://app.netlify.com/start/deploy?repository=https://gitlab.com/shrmpy/hello-activitypub">
    <img src="https://www.netlify.com/img/deploy/button.svg"/></a>


### Requirements
- Netlify account
- Discord webhook (OPTIONAL)


## Credits
Reference AP Inbox
  by [Darius Kazemi](https://github.com/dariusk/express-activitypub) ([LICENSE](https://github.com/dariusk/express-activitypub/blob/master/LICENSE-MIT))

Kotori Netlify
  by [Musakai](https://github.com/musakui/kotori-netlify) ([LICENSE](https://github.com/musakui/kotori/blob/omo/LICENSE))

Basic AP Server
  by [Eugen Rochko](https://blog.joinmastodon.org/2018/06/how-to-implement-a-basic-activitypub-server/)

AP Inbox
  by [Eugen Rochko](https://blog.joinmastodon.org/2018/07/how-to-make-friends-and-verify-requests/)

